<?php
class Database
{
    protected $connection = null;
 
    public function __construct()
    {
        try {
            $this->connection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE_NAME);
         
            if ( mysqli_connect_errno()) {
                throw new Exception("No fue posible contectar a la base de datos.");   
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());   
        }           
    }
 
    public function insertUser($query, $nombre, $apellido, $correo, $contraseña, $rol)
    { //var_dump($query);exit;
        try {
            $stmt = $this->connection->prepare( $query );
 
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
 
            $stmt->bind_param("sssss", $nombre, $apellido, $correo, $contraseña, $rol);
 
            $stmt->execute();

            $stmt->close();

            return "Insertado con exito.";
        }catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        } 
    }

    public function login($query, $username, $password)
    { 
        try {
            $stmt = $this->connection->prepare( $query );
 
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
            //var_dump($username);exit;
            $stmt->bind_param("ss", $username, $password);
 
            $stmt->execute();

            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);

            $stmt->close();

            return $result;
        }catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }
    }

    public function post($query, $titulo, $descripcion, $fecha_publicacion, $usuario)
    { 
        try {
            $stmt = $this->connection->prepare( $query );
 
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
 
            $stmt->bind_param("ssss", $titulo, $descripcion, $fecha_publicacion, $usuario);
 
            $stmt->execute();

            $stmt->close();

            return "Insertado con exito.";
        }catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        } 
    }

    public function update($query, $id, $titulo, $descripcion)
    { 
        try {
            $stmt = $this->connection->prepare( $query );
 
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
 
            $stmt->bind_param("ssi", $titulo, $descripcion, $id);
 
            $stmt->execute();

            $stmt->close();

            return "Actualizado con exito.";
        }catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        } 
    }

    public function delete($query, $id)
    { 
        try {
            $stmt = $this->connection->prepare( $query );
 
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
 
            $stmt->bind_param("i", $id);
 
            $stmt->execute();

            $stmt->close();

            return "Eliminado con exito.";
        }catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        } 
    }

    public function show($query)
    { 
        try {
            $stmt = $this->connection->prepare( $query );
 
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
 
            //$stmt->bind_param("i", $id);
 
            $stmt->execute();

            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);

            $stmt->close();

            return $result;
        }catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        } 
    }

    public function select($query = "" , $params = [])
    {
        try {
            $stmt = $this->executeStatement( $query , $params ); 
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);              
            $stmt->close();
 
            return $result;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }
        return false;
    }
 
    private function executeStatement($query = "" , $params = [])
    {
        try {
            $stmt = $this->connection->prepare( $query );
 
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
 
            if( $params ) {
                $stmt->bind_param($params[0], $params[1]);
            }
 
            $stmt->execute();
 
            return $stmt;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }   
    }
}