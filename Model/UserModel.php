<?php
require_once PROJECT_ROOT_PATH . "/Model/Database.php";
 
class UserModel extends Database
{
    public function getUsers($limit)
    {
        return $this->select("SELECT * FROM usuario ORDER BY id ASC LIMIT ?", ["i", $limit]);
    }

    public function registerUser($nombre, $apellido, $correo, $contraseña, $rol)
    {
        //$params = array( $nombre, $apellido, $correo, $contraseña, $rol);
        return $this->insertUser("INSERT INTO usuario (nombre, apellido, correo, contrasena, rol) VALUES (?, ?, ?, ?, ?)",  $nombre, $apellido, $correo, $contraseña, $rol);
    }

    public function loginUser($username, $password)
    {
        return $this->login("SELECT * FROM usuario WHERE correo LIKE ? AND contrasena LIKE ?", $username, $password);
    }

    public function postBlog($titulo, $descripcion, $fecha_publicacion, $usuario)
    {
        return $this->post("INSERT INTO publicacion (titulo, descripcion, fecha_publicacion, usuario) VALUES (?, ?, ?, ?)", $titulo, $descripcion, $fecha_publicacion, $usuario);
    }

    public function updateBlog($id, $titulo, $descripcion)
    {
        return $this->update("UPDATE publicacion SET titulo = ?, descripcion = ? WHERE id = ?", $id, $titulo, $descripcion);
    }

    public function deleteBlog($id)
    {
        return $this->delete("UPDATE publicacion SET activo = 0 WHERE id = ?", $id);
    }

    public function showBlogs()
    {
        return $this->show("SELECT p.titulo as 'Título', p.descripcion as 'Descripcion', p.fecha_publicacion as 'Fecha publicación', concat(u.nombre,' ', u.apellido) as 'Usuario', r.nombre as 'Rol' FROM blog.usuario as u inner join blog.publicacion as p on u.id=p.usuario inner join blog.rol as r on u.rol=r.id where p.activo = 1");
    }
}