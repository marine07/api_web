<?php
require_once __DIR__ . "/../../Model/UserModel.php";;
class UserController extends BaseController
{
    /**
     * "/user/list" Endpoint - Get list of users
     */
    public function listAction($limit)
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];
        $arrQueryStringParams = $this->getQueryStringParams();
 
        if (strtoupper($requestMethod) == 'GET') {
            try {
                //echo __DIR__ . "/../../Model/UserModel.php";
                $userModel = new UserModel();
 
                //$intLimit = 10;
                $intLimit = $limit;
                if (isset($arrQueryStringParams['limit']) && $arrQueryStringParams['limit']) {
                    $intLimit = $arrQueryStringParams['limit'];
                }
                $arrUsers = $userModel->getUsers($intLimit);
                $responseData = json_encode($arrUsers);
            } catch (Error $e) {
                $strErrorDesc = $e->getMessage().'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }
 
        // send output
        if (!$strErrorDesc) {
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(json_encode(array('error' => $strErrorDesc)), 
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    public function registerUser()
    {
        //echo $requestMethod = $_SERVER["REQUEST_METHOD"];
        //var_dump($_POST);
        //exit;

        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'POST') {
            try {
                //echo __DIR__ . "/../../Model/UserModel.php";
                $userModel = new UserModel();
 
                $arrUsers = $userModel->registerUser($_POST["nombre"],$_POST["apellido"],$_POST["correo"],$_POST["contrasena"],$_POST["rol"]);
                //$responseData = json_encode($arrUsers);

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage().'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        // send output
        if (!$strErrorDesc) {
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(json_encode(array('error' => $strErrorDesc)), 
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    public function loginUser()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];
        
        if (strtoupper($requestMethod) == 'POST') {
            try {
                $userModel = new UserModel();
 
                $arrUsers = $userModel->loginUser($_POST["username"],$_POST["password"]);
                //$responseData = json_encode($arrUsers);
                //var_dump($_POST);exit;
                //var_dump($arrUsers[0]['nombre']);exit;
                //var_dump($arrUsers);exit;

                //var_dump(bin2hex($arrUsers[0]['nombre'].$arrUsers[0]['apellido'].date('Y-m-d')));exit;
                $token = bin2hex($arrUsers[0]['nombre'].$arrUsers[0]['apellido'].date('Y-m-d'));
                $_SESSION["token"] = $token;
                $_SESSION["id_user"] = $arrUsers[0]['id'];
                $_SESSION["rol_user"] = $arrUsers[0]['rol'];
                $responseData = json_encode(["TOKEN" => $token]);
            } catch (Error $e) {
                $strErrorDesc = $e->getMessage().'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        // send output
        if (!$strErrorDesc) {
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(json_encode(array('error' => $strErrorDesc)), 
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    public function post()
    {
        //echo $requestMethod = $_SERVER["REQUEST_METHOD"];
        //var_dump($_POST);
        //exit;

        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'POST') {
            try {
                //echo __DIR__ . "/../../Model/UserModel.php";
                $userModel = new UserModel();
 
                $arrUsers = $userModel->postBlog($_POST["titulo"],$_POST["comentario"],date('Y-m-d'),$_SESSION["id_user"]);
                $responseData = json_encode($arrUsers);

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage().'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        // send output
        if (!$strErrorDesc) {
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(json_encode(array('error' => $strErrorDesc)), 
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    public function update()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'POST') {
            try {
                //echo __DIR__ . "/../../Model/UserModel.php";
                $userModel = new UserModel();
 
                $arrUsers = $userModel->updateBlog($_POST["id"],$_POST["titulo"],$_POST["comentario"]);
                $responseData = json_encode($arrUsers);

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage().'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        // send output
        if (!$strErrorDesc) {
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(json_encode(array('error' => $strErrorDesc)), 
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    public function delete()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];

        if (strtoupper($requestMethod) == 'POST') {
            try {
                //echo __DIR__ . "/../../Model/UserModel.php";
                $userModel = new UserModel();
 
                $arrUsers = $userModel->deleteBlog($_POST["id"]);
                $responseData = json_encode($arrUsers);

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage().'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        // send output
        if (!$strErrorDesc) {
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(json_encode(array('error' => $strErrorDesc)), 
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    public function show()
    {
        $strErrorDesc = '';
        $requestMethod = $_SERVER["REQUEST_METHOD"];
        
        if (strtoupper($requestMethod) == 'POST') {
            try {
                $userModel = new UserModel();

                $arrUsers = $userModel->showBlogs();
                $responseData = json_encode($arrUsers);

            } catch (Error $e) {
                $strErrorDesc = $e->getMessage().'Something went wrong! Please contact support.';
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';
            }
        } else {
            $strErrorDesc = 'Method not supported';
            $strErrorHeader = 'HTTP/1.1 422 Unprocessable Entity';
        }

        // send output
        if (!$strErrorDesc) {
            $this->sendOutput(
                $responseData,
                array('Content-Type: application/json', 'HTTP/1.1 200 OK')
            );
        } else {
            $this->sendOutput(json_encode(array('error' => $strErrorDesc)), 
                array('Content-Type: application/json', $strErrorHeader)
            );
        }
    }

    public function validarRol($ruta)
    {
        $result = Null;

        switch($ruta){
            case 'publicar': 
                $result = array_search($_SESSION["rol_user"], array(3,4,5));
            break;
            case 'actualizar': 
                $result = array_search($_SESSION["rol_user"], array(4,5));
            break;
            case 'borrar': 
                $result = array_search($_SESSION["rol_user"], array(5));
            break;
            case 'mostrar': 
                $result = array_search($_SESSION["rol_user"], array(2,3,4,5));
            break;
        }

        return $result;

    }
}